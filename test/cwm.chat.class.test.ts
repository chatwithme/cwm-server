import { CWMChat } from "../src";
import { faker } from '@faker-js/faker';
import * as dotenv from 'dotenv'

dotenv.config()

const TestToken = process.env.APP_TOKEN
const USERS_DATAS = [
  {
    userId: faker.datatype.number({min: 0, max: 100}),
    userName: faker.internet.userName()
  },
  {
    userId: faker.datatype.uuid(),
    userName: faker.internet.userName()
  }
]


describe('Test CWMChat class', () => {

  test('appToken setter and getter', () => {
    CWMChat.appToken = TestToken
    expect(CWMChat.appToken).toBe(TestToken)
  })

  test('tokenGeneration with no token', async () => {
    CWMChat.appToken = ""

    const userData = {
      userId: faker.datatype.uuid(),
      userName: faker.internet.userName()
    }
    expect(CWMChat.generateCWMUserToken(userData)).rejects.toEqual({error: "The token should be set"})
  })

  test('tokenGeneration with no token', async () => {
    CWMChat.appToken = faker.datatype.uuid();

    const userData = {
      userId: faker.datatype.uuid(),
      userName: faker.internet.userName()
    }

    try {
      const token = await CWMChat.generateCWMUserToken(userData)
    } catch (e) {
      expect(e.response.status).toEqual(401)
    }
  })

  test('tokenGeneration with valid data', async () => {
    CWMChat.appToken = TestToken

    for (const userData of USERS_DATAS) {
      const token = await CWMChat.generateCWMUserToken(userData)

      expect(token).not.toBeNull()
    }
  })

})

