# ChatWithMe Back Library
## Installation
Firstly install the package with the command below in a nodejs project.
```shell
npm install @chatwithme/server
```

## Setup your ChatWithMe token

You can grab it on [chatwithme.fr](https://chatwithme.fr).
For more information: [quickstart](https://doc.chatwithme.fr/docs/getting-started/cwm-intro)

```typescript
import { CWMChat } from "@chatwithme/server";

CWMChat.appToken = process.env.CHAT_TOKEN
```

## Retrieve token for your user
```ts
CWMChat.generateCWMUserToken({
    userId: "<your user id>",
    userName: "<your user name>"
}).then(token => {
    console.log(token)
}).catch(() => {
    // Handle error
})
```

## Create a POST route in your back end
Here a simple example with [express](https://www.npmjs.com/package/express).
```ts
import { CWMChat } from "@chatwithme/server";

app.post('/chat', (req, res) => {
  
    const user = req.user;
  
    if(user == undefined) {
        res.status(400).json({ message: "You are not logged."})
    }

    CWMChat.generateCWMUserToken({
        userId: user.id,
        userName: name
    }).then(token => {
        res.json(token)
    }).catch(() => {
        // Handle error
    })
})
```

And it's done !

## @chatwithme/client
After that you'll need to use another of our package in your front-end [@chatwithme/client](https://www.npmjs.com/package/@chatwithme/client) if it's not done yet.
