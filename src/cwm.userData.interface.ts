export interface CWMUserData {
    userId: number | string;
    userName: string;
}