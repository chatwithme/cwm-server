import { CWMChat } from "./cwm.chat.class";
import * as dotenv from 'dotenv'
import { faker } from "@faker-js/faker";

dotenv.config()

CWMChat.appToken = faker.datatype.uuid() // process.env.APP_TOKEN
CWMChat.generateCWMUserToken({
  userId: faker.datatype.number(5),
  userName: faker.internet.userName()
}).then((token) => {
  console.log(token)
}).catch((err) => {
  console.log(err.response.status)
})
