import axios, {AxiosResponse} from 'axios'
import {CWMUserData} from "./cwm.userData.interface";
import {CWMUserToken} from "./cwm.userToken.interface";

export class CWMChat {

    private _appToken: string
    private _CWMEndpoint: string
    private static _instance: CWMChat | null = null

    public static get appToken(): string {
        return this.instance._appToken
    }

    public static set appToken(appToken: string) {
        this.instance._appToken = appToken
    }
    constructor () {
        this._CWMEndpoint = process.env.CWM_LINK == undefined ? 'https://api.chatwithme.fr/webhooks/user-token' : process.env.CWM_LINK
    }

    protected static get instance(): CWMChat {
        if (CWMChat._instance == null) {
            CWMChat._instance = new CWMChat()
        }
        return CWMChat._instance
    }

    public static generateCWMUserToken(userData: CWMUserData): Promise<CWMUserToken> {
        return new Promise<CWMUserToken>((resolve, reject) => {
            if (this.instance._appToken == undefined || this.instance._appToken == ""){
                reject({error: "The token should be set"})
            }
            axios.post(
                this.instance._CWMEndpoint,
                {
                    externalUserId: userData.userId,
                    externalUserUsername: userData.userName
                },
                {
                    headers: {
                        Authorization: 'Bearer ' + this.appToken
                    }
                }
            ).then((data: AxiosResponse) => {
                resolve(data.data)
            }).catch(reject)
        })
    }
}
